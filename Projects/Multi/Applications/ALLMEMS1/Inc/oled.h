/*
 * oled.h
 *
 *  Created on: 23/10/2019
 *      Author: user
 */

#ifndef OLED_H_
#define OLED_H_

#include "main.h"
#include "SensorTile.h"

enum{WHITE, BLACK, INVERSE};
uint8_t *bufferOled;

extern I2C_HandleTypeDef I2C_SENSORTILE_Handle;

void displayOled(void);
void clearDisplay(void);
void fillDisplay(void);

void drawPixel(int16_t x, int16_t y, uint16_t color);

uint8_t oled_init();

void ssd1306_command1(uint8_t c);
void ssd1306_sendList(const uint8_t *c, uint8_t n);
uint8_t write_oled(uint8_t slave_addr, uint8_t* pData, uint16_t sizeOfBuffer);
void writeBuffer(uint8_t data);

#endif /* OLED_H_ */

