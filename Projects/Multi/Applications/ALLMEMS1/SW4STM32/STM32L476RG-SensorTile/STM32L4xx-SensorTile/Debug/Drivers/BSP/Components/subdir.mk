################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221/HTS221_Driver.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221/HTS221_Driver_HL.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb/LPS22HB_Driver.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb/LPS22HB_Driver_HL.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_ACC_driver.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_ACC_driver_HL.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_MAG_driver.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_MAG_driver_HL.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm/LSM6DSM_ACC_GYRO_driver.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm/LSM6DSM_ACC_GYRO_driver_HL.c \
C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115/STC3115_Driver.c 

OBJS += \
./Drivers/BSP/Components/HTS221_Driver.o \
./Drivers/BSP/Components/HTS221_Driver_HL.o \
./Drivers/BSP/Components/LPS22HB_Driver.o \
./Drivers/BSP/Components/LPS22HB_Driver_HL.o \
./Drivers/BSP/Components/LSM303AGR_ACC_driver.o \
./Drivers/BSP/Components/LSM303AGR_ACC_driver_HL.o \
./Drivers/BSP/Components/LSM303AGR_MAG_driver.o \
./Drivers/BSP/Components/LSM303AGR_MAG_driver_HL.o \
./Drivers/BSP/Components/LSM6DSM_ACC_GYRO_driver.o \
./Drivers/BSP/Components/LSM6DSM_ACC_GYRO_driver_HL.o \
./Drivers/BSP/Components/STC3115_Driver.o 

C_DEPS += \
./Drivers/BSP/Components/HTS221_Driver.d \
./Drivers/BSP/Components/HTS221_Driver_HL.d \
./Drivers/BSP/Components/LPS22HB_Driver.d \
./Drivers/BSP/Components/LPS22HB_Driver_HL.d \
./Drivers/BSP/Components/LSM303AGR_ACC_driver.d \
./Drivers/BSP/Components/LSM303AGR_ACC_driver_HL.d \
./Drivers/BSP/Components/LSM303AGR_MAG_driver.d \
./Drivers/BSP/Components/LSM303AGR_MAG_driver_HL.d \
./Drivers/BSP/Components/LSM6DSM_ACC_GYRO_driver.d \
./Drivers/BSP/Components/LSM6DSM_ACC_GYRO_driver_HL.d \
./Drivers/BSP/Components/STC3115_Driver.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/Components/HTS221_Driver.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221/HTS221_Driver.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/HTS221_Driver_HL.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221/HTS221_Driver_HL.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LPS22HB_Driver.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb/LPS22HB_Driver.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LPS22HB_Driver_HL.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb/LPS22HB_Driver_HL.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LSM303AGR_ACC_driver.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_ACC_driver.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LSM303AGR_ACC_driver_HL.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_ACC_driver_HL.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LSM303AGR_MAG_driver.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_MAG_driver.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LSM303AGR_MAG_driver_HL.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr/LSM303AGR_MAG_driver_HL.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LSM6DSM_ACC_GYRO_driver.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm/LSM6DSM_ACC_GYRO_driver.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/LSM6DSM_ACC_GYRO_driver_HL.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm/LSM6DSM_ACC_GYRO_driver_HL.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/BSP/Components/STC3115_Driver.o: C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115/STC3115_Driver.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


