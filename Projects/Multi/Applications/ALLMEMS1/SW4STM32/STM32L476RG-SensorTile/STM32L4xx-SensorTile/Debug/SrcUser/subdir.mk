################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SrcUser/oled.c \
../SrcUser/ssd1306_fonts.c \
../SrcUser/ssd1306_tests.c \
../SrcUser/ssd_1306.c 

OBJS += \
./SrcUser/oled.o \
./SrcUser/ssd1306_fonts.o \
./SrcUser/ssd1306_tests.o \
./SrcUser/ssd_1306.o 

C_DEPS += \
./SrcUser/oled.d \
./SrcUser/ssd1306_fonts.d \
./SrcUser/ssd1306_tests.d \
./SrcUser/ssd_1306.d 


# Each subdirectory must supply rules for building sources it contributes
SrcUser/%.o: ../SrcUser/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 -DUSE_HAL_DRIVER -DSTM32_SENSORTILE -DSTM32L476xx -DUSE_STM32L4XX_NUCLEO -DALLMEMS1 -I"C:/Users/USER/workspace/pedometer/Projects/Multi/Applications/ALLMEMS1/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/SensorTile" -I"C:/Users/USER/workspace/pedometer/Drivers/CMSIS/Include" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/Common" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/SimpleBlueNRG_HCI/includes" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueNRG/Interface" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm6dsm" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/hts221" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lps22hb" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/lsm303agr" -I"C:/Users/USER/workspace/pedometer/Drivers/BSP/Components/stc3115" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionAR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionCP_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionFX_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MotionGR_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_BlueVoiceADPCM_Library/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_MetaDataManager" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"C:/Users/USER/workspace/pedometer/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


