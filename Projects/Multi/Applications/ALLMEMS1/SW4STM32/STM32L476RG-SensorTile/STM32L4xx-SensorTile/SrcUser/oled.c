/*
 * oled.c
 *
 *  Created on: 23/10/2019
 *      Author: user
 */

#include "main.h"
#include "oled.h"
#include "stm32l4xx_hal_i2c.h"

uint8_t txBuffer[255];
uint8_t txBufferIndex = 0;
uint8_t txBufferLength = 0;
static const uint8_t WIDTH_SSDOLED = 128;
static const uint8_t HEIGHT_SSDOLED = 64;

const uint8_t dlist1[] = {
		0x22, //SSD1306_PAGEADDR = 0x22;
		0,
		0xFF,
		0x21, //SSD1306_COLUMNADDR = 0x21;
		0 };

void displayOled(void){
	ssd1306_sendList(dlist1, sizeof(dlist1));
	ssd1306_command1(WIDTH_SSDOLED - 1);

	uint16_t count = WIDTH_SSDOLED * ((HEIGHT_SSDOLED + 7) / 8);
	uint8_t *ptr   = bufferOled;
	    //wire->beginTransmission(i2caddr);
	    txBufferIndex = 0;
	    txBufferLength = 0;
	    writeBuffer((uint8_t)0x40); //wire->write
	    uint8_t bytesOut = 1;
	    while(count--) {
	      if(bytesOut >= 255) {
	        //wire->endTransmission();
	        write_oled(0x78, txBuffer, txBufferLength);
	    	//wire->beginTransmission(i2caddr);
	        writeBuffer((uint8_t)0x40);
	        bytesOut = 1;
	      }
	      writeBuffer(*ptr++);
	      bytesOut++;
	    }
	    //wire->endTransmission();
	    write_oled(0x78,txBuffer, txBufferLength);
}

void clearDisplay(void){
	memset(bufferOled, 0, WIDTH_SSDOLED * ((HEIGHT_SSDOLED + 7) / 8));
}

void fillDisplay(void){
	memset(bufferOled, 1, WIDTH_SSDOLED * ((HEIGHT_SSDOLED + 7) / 8));
}

void drawPixel(int16_t x, int16_t y, uint16_t color){
	if((x >= 0) && (x < WIDTH_SSDOLED) && (y >= 0) && (y < HEIGHT_SSDOLED)){
		x = WIDTH_SSDOLED - x - 1;
		y = HEIGHT_SSDOLED - y - 1;
	}
	switch(color){
	case WHITE:
		bufferOled[x + (y/8) * WIDTH_SSDOLED] |= (1 << (y&7));
		break;
	}
}

uint8_t oled_init(){
	if((!bufferOled) && !(bufferOled = (uint8_t *)malloc(WIDTH_SSDOLED * ((HEIGHT_SSDOLED + 7) / 8)))){
		return 0;
	}
	clearDisplay();
	return 1;
}

void ssd1306_command1(uint8_t c) {
	uint8_t intBuffer[2] = {0x00, c};
	write_oled((0x78), intBuffer, 2);
}

// Issue list of commands to SSD1306, same rules as above re: transactions.
// This is a private function, not exposed.
void ssd1306_sendList(const uint8_t *c, uint8_t n) {
    //wire->beginTransmission(i2caddr);
	txBufferIndex = 0;
	txBufferLength = 0;
    writeBuffer(0x00); // Co = 0, D/C = 0
    uint8_t bytesOut = 1;
    while(n--) {
      if(bytesOut >= 255) {
        //wire->endTransmission();
    	  write_oled(0x78,txBuffer, txBufferLength);
        //wire->beginTransmission(i2caddr);
        writeBuffer(0x00); // Co = 0, D/C = 0
        bytesOut = 1;
      }
      writeBuffer(c);
      c++;
      bytesOut++;
    }
    //wire->endTransmission();
    write_oled(0x78, txBuffer, txBufferLength);
}

uint8_t write_oled(uint8_t slave_addr, uint8_t* pData, uint16_t sizeOfBuffer){
	slave_addr = slave_addr<<1;
	HAL_StatusTypeDef status = HAL_OK;
	status = HAL_I2C_Master_Transmit(&I2C_SENSORTILE_Handle, slave_addr, pData, sizeOfBuffer, 2000);

	for(uint8_t i = 0; i < 255; i++){
		txBuffer[i] = 0;
	}
	txBufferIndex = 0;
	txBufferLength = 0;
	return status;
}

/*
void writeBuffer(uint8_t data){
	txBuffer[txBufferIndex] = data;
<<<<<<< HEAD
	txBufferLength = ++txBufferIndex;
=======
	txBufferIndex++;
	txBufferLength = txBufferIndex;
>>>>>>> master
}*/


